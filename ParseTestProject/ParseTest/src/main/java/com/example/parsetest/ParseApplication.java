package com.example.parsetest;

import android.app.Application;


import com.parse.Parse;
import com.parse.ParseACL;

import com.parse.ParseUser;

import android.app.Application;
/**
 * Created by dev1 on 2/10/14.
 */
public class ParseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // Add your initialization code here
        Parse.initialize(this, "fnRFxpcF4e5WrgcuOpSOP8s3YGQW0bgcwdbj4dCy", "NqGdOHPiLAlZWSsxG3hwfWSfkj0JHcu7pUmyAWHw");


        ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();

        // If you would like all objects to be private by default, remove this line.
        defaultACL.setPublicReadAccess(true);

        ParseACL.setDefaultACL(defaultACL, true);
    }

}